python-pyo (1.0.5-5) unstable; urgency=medium

  * Adding back E-Pyo editor. 

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Wed, 20 Sep 2023 10:38:11 -0400

python-pyo (1.0.5-4) unstable; urgency=medium

  * Update watch file to reflect new upstream VCS.
  * Update Homepage field in control
  * Update Standards-Version to 4.6.2

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Wed, 13 Sep 2023 03:53:37 -0400

python-pyo (1.0.5-3) unstable; urgency=medium

  * Quick fix for unit test for 32-bit archs. Closes: #1038126.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Fri, 11 Aug 2023 10:29:48 -0400

python-pyo (1.0.5-2) unstable; urgency=medium

  * Add autopkgtest-pkg-pybuild to call upstream tests in ./tests/pytests
  * Add patch which fixes a test from upstream
  * Add patch which removes an upstream test requiring an audio card

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Tue, 28 Mar 2023 14:39:33 -0400

python-pyo (1.0.5-1) unstable; urgency=medium

  * New upstream version.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Mon, 27 Mar 2023 06:54:23 -0400

python-pyo (1.0.4-1) unstable; urgency=medium

  * New upstream version.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Tue, 17 Aug 2021 10:26:57 -0400

python-pyo (1.0.3-0.1) unstable; urgency=medium

  * New upstream version.
  * Fix build with python 3.9

 -- Matthias Klose <doko@debian.org>  Thu, 15 Oct 2020 10:48:52 +0200

python-pyo (1.0.2-1) unstable; urgency=medium

  * New upstream release.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Wed, 19 Aug 2020 06:58:42 -0400

python-pyo (1.0.0-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Drop python2 support; Closes: #938081

 -- Sandro Tosi <morph@debian.org>  Fri, 10 Jan 2020 20:23:01 -0500

python-pyo (1.0.0-2) unstable; urgency=medium

  * Fix epyo script overwriting.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Sat, 05 Oct 2019 14:58:39 -0400

python-pyo (1.0.0-1) unstable; urgency=medium

  * First stable upstream release!

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Sat, 28 Sep 2019 22:29:58 -0400

python-pyo (0.9.0-2) unstable; urgency=medium

  * Enable automatic-dbgsym. (Closes: #885988)

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Fri, 27 Jul 2018 08:38:36 -0400

python-pyo (0.9.0-1) unstable; urgency=medium

  * New upstream release.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Sat, 21 Apr 2018 16:35:40 -0300

python-pyo (0.8.8-1) unstable; urgency=medium

  * New upstream release.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Thu, 16 Nov 2017 10:00:03 -0500

python-pyo (0.8.7-1) unstable; urgency=medium

  * New upstream release.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Thu, 31 Aug 2017 07:09:00 -0400

python-pyo (0.8.6-1) unstable; urgency=medium

  * New upstream release.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Tue, 09 May 2017 20:21:49 -0400

python-pyo (0.8.4-1) unstable; urgency=medium

  * New upstream release.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Tue, 28 Mar 2017 12:16:45 -0400

python-pyo (0.8.3+git20170311.01-1) unstable; urgency=medium

  * Added a setKeepLast method to TableRead object (will hold last value).

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Sun, 12 Mar 2017 17:36:32 -0400

python-pyo (0.8.3-1) unstable; urgency=medium

  * New upstream release.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Thu, 02 Mar 2017 11:08:16 -0500

python-pyo (0.8.2-1) unstable; urgency=medium

  * New upstream release.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Sat, 17 Dec 2016 22:02:35 -0500

python-pyo (0.8.1-1) unstable; urgency=medium

  * New upstream release.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Tue, 13 Dec 2016 13:18:31 -0500

python-pyo (0.7.9-1) unstable; urgency=medium

  * New upstream release.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Mon, 07 Mar 2016 15:13:08 -0500

python-pyo (0.7.8+git20160129-1) unstable; urgency=medium

  * Fix segfault when rendering.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Fri, 29 Jan 2016 14:03:47 -0500

python-pyo (0.7.8-1) unstable; urgency=medium

  * New upstream release.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Tue, 19 Jan 2016 09:50:12 -0500

python-pyo (0.7.6+git20150826.1f0dc1aa93-1) unstable; urgency=medium

  * Added new objects: MidiListener and OscListener

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Wed, 26 Aug 2015 00:51:50 -0400

python-pyo (0.7.6-1) unstable; urgency=medium

  * New upstream release.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Tue, 28 Jul 2015 17:02:57 -0400

python-pyo (0.7.5-1) experimental; urgency=medium

  * New upstream version.
  * License changed from GPL to LGPL
  * Build-depends on libjack-jackd2-dev (was libjack-dev)
  * Recommends jackd2

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Mon, 23 Mar 2015 20:16:04 -0400

python-pyo (0.7.4-1) experimental; urgency=medium

  * New upstream version.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Thu, 29 Jan 2015 22:49:12 -0500

python-pyo (0.7-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Updates for wxPython 3.0 compatibility (Closes: #759092):
    - Fix deprecation warnings with wxPython 3.0.
    - Fix check for wxPython not to insist that 2.8 is installed before picking
      something that's 2.8 or newer.
    - Update uses of removed alias wx.Color to wx.Colour.
    - Update FileDialog flags to use the non-deprecated "FD_" prefixed form.

 -- Olly Betts <olly@survex.com>  Sat, 20 Sep 2014 01:34:09 +0000

python-pyo (0.7-2) unstable; urgency=low

  * Depends on python-wxgtk3.0 instead of python-wxgtk2.8 to have the
    graphical IDE (E-Pyo) working.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Sat, 30 Aug 2014 20:13:47 -0400

python-pyo (0.7-1) unstable; urgency=medium

  * New upstream version.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Sat, 30 Aug 2014 17:12:30 -0400

python-pyo (0.6.9-1) unstable; urgency=medium

  * New upstream version.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Sun, 13 Apr 2014 00:15:48 -0400

python-pyo (0.6.8-1) unstable; urgency=low

  * New upstream version.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Sat, 26 Oct 2013 10:35:58 -0400

python-pyo (0.6.6+svn1132-2) unstable; urgency=low

  * Don't compress sample audio files.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Thu, 05 Sep 2013 10:19:57 -0400

python-pyo (0.6.6+svn1132-1) unstable; urgency=low

  * Added new objects:
    - PVMix, Mix the most prominent components from two phase vocoder streaming
      objects.
    - TableScale, Scales values from a table and writes them into another table.
    - Granule, another granular synthesis generator.
    - PVBufTabLoops, phase vocoder buffer with bin independent speed playback.
    - PVBufLoops, phase vocoder buffer with bin independent speed playback.
    - PVShift, spectral frequency shifter. PVAmpMod and PVFreqMod, frequency
      independent modulations.
    - PVDelay, spectral delays and PVBuffer, pv recorder and playback.
    - PVFilter. Spectral filtering.
    - PVCross, PVMult, PVMorph. Spectral morphing.
    - PVAddSynth, Phase Vocoder additive synthesis object.
  * Added E-Pyo binary to the package, accessible via Sound & Video menu.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Mon, 02 Sep 2013 17:25:57 -0400

python-pyo (0.6.6+svn1108-1) unstable; urgency=low

  * New upstream commits adding 6 new filters and a couple of bug fixes.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Tue, 28 May 2013 14:27:12 -0400

python-pyo (0.6.6-1) unstable; urgency=low

  * New upstream release. It includes two new objects: CvlVerb: convolution
    based  multi-channel reverberation and Spectrum: spectrum analyzer with
    multi-channel display.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Tue, 14 May 2013 21:37:45 -0400

python-pyo (0.6.4-1) unstable; urgency=low

  * New upstream release.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Tue, 26 Feb 2013 23:40:59 -0500

python-pyo (0.6.3+svn1068-1) unstable; urgency=low

  * New upstream release.
  * Start tracking upstream svn.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Wed, 30 Jan 2013 00:41:56 -0500

python-pyo (0.6.2-1) unstable; urgency=low

  * New upstream release.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Tue, 03 Jul 2012 23:45:41 -0400

python-pyo (0.6.1-1) unstable; urgency=low

  * Initial release. (Closes: #676712)

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Fri, 08 Jun 2012 20:35:45 -0400
